class AddInterviewToAppointments < ActiveRecord::Migration[5.1]
  def change
    add_reference :appointments, :interview, foreign_key: true
  end
end
