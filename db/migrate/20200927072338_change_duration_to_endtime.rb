class ChangeDurationToEndtime < ActiveRecord::Migration[5.1]
  def change
    rename_column :interviews, :duration, :endtime
  end
end
