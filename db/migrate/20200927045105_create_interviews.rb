class CreateInterviews < ActiveRecord::Migration[5.1]
  def change
    create_table :interviews do |t|
      t.date :date
      t.time :starttime
      t.time :duration

      t.timestamps
    end
  end
end
