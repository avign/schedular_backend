class RemoveTimes < ActiveRecord::Migration[5.1]
  def change
    remove_column :interviews , :starttime
    remove_column :interviews , :endtime
  end
end
