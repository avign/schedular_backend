class User < ApplicationRecord
    has_many :appointments
    has_many :interviews, through: :appointments
end
