class UserController < ApplicationController

    def index
        @users = User.all
        render json: @users
    end
    def show
        render json: User.find(params[:id])
    end

    def create
        @user = User.new(user_params)
        if @user.save
            render json: @user
        else
            render error: { error: "couldnot create user" }, status: 400
        end
    end

    def update
        @user = User.find(params[:id])
        @user.update(user_params)
        render json: { message: "user updated succesfully" }, status: 200
    end

    def destroy
        @user = User.find(params[:id])
        @user.destroy
        render json: { message: "user deleted succesfully" }, status: 200
    end

    private
    def user_params
        params.permit( :name, :email )
    end

end

