class InterviewController < ApplicationController
    
    def index
        @interviews = Interview.all
        render json: @interviews
    end

    def show
        render json: Interview.find(params[:id])
    end

    def create
        @user1 = User.find(params[:user1_id])
        @user2 = User.find(params[:user2_id]);
        
        interview_date = params[:date].to_date
        interview_starttime = params[:starttime].to_datetime
        interview_endtime = params[:endtime].to_datetime

        user1_interviews = @user1.interviews;
        user2_interviews = @user2.interviews;

        user1_has_conflict = false;
        user2_has_conflict = false;
     
        user1_interviews.each do |interview|   
            if !(interview_endtime < interview.starttime || interview_starttime > interview.endtime) 
                user1_has_conflict = !user1_has_conflict
            end
        end

        user2_interviews.each do |interview|
            if !(interview_endtime < interview.starttime || interview_starttime > interview.endtime) 
                user2_has_conflict = !user2_has_conflict
            end 
        end

        if(!user1_has_conflict and !user2_has_conflict) 
            @interview = Interview.create(interview_params)
            @interview.users << @user1
            @interview.users << @user2
            appointments = @interview.appointments
            appointments.each do |appointment|
                if appointment.user_id == @user1.id
                    appointment.role = params[:user1_role]
                else
                    appointment.role = params[:user2_role]
                end
                appointment.save!
            end
            render json: @interview
        else
            render json: { message: "there is a conflicting time" }, status: 400
        end
    end

    private
    def interview_params
        new_params = params.require(:interview).permit( :starttime, :endtime, :date)
        new_params[:starttime] = new_params[:starttime].to_datetime
        new_params[:endtime] = new_params[:endtime].to_datetime
        new_params[:date] = new_params[:date].to_date
        return new_params
    end

end
